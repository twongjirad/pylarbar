#!/user/bin/env python

from distutils.core import setup

setup(name='pylarbar',
      vesion='0.1',
      description='Numerical model of acrylic light guide bars used for LArTPCs',
      author='Taritree Wongjirad',
      author_email='taitree@mit.edu',
      packages=['pylarbar'],
      package_data={'pylarbar':['cl/*','Random123/*']},
      )
