# genphotons: module to generate photon at certain position and random direction

# This file is part of pylarbar
# Copyright 2015 by Taritree Wongjirad (taritree@mit.edu)

# pylarbar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# pylarbar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pylarbar.  If not, see <http://www.gnu.org/licenses/>.

import os,sys
import numpy as np
import photon as ph
import geomobjs as go
import pyopencl as cl

def photongen_at_source_gpu( ctx, queue, nphotons, pos, rand_seed, copy_to_host=False ):
    dir_np = np.zeros( 3*nphotons, dtype=np.float32 )

    mf = cl.mem_flags

    dir_g = cl.Buffer( ctx, mf.READ_WRITE, dir_np.nbytes )

    kernelfilepath = os.path.dirname(__file__)+"/cl/genphotons.cl"
    kernelfile = open(kernelfilepath,'r')

    cflags = ["-I"+os.path.dirname(__file__)]
    prg = cl.Program(ctx, kernelfile.read() ).build( options=cflags )

    workdims = ( nphotons,1,1 )
    prg.gen_photon_dir( queue, workdims, None,
                        np.uint32(rand_seed),
                        dir_g )
    if copy_to_host:
        cl.enqueue_copy( queue, dir_np, dir_g )

    return dir_g, dir_np

def generate_photons_on_bar( location, nphotons, vol, dir_list=None ):
    photons = []
    for n in xrange(0,nphotons):
        costh = np.random.random()-1.0 # [-1,0)
        phi = np.random.random()*2*np.pi # [0,2pi)
        if dir_list==None:
            dir = np.array( [np.cos(phi)*np.sqrt(1-costh*costh), np.sin(phi)*np.sqrt(1-costh*costh), costh ] )
        else:
            dir = dir_list.reshape(nphotons,3)[n,:]
        ray = go.ray( location, dir )
        photons.append( ph.photon( ray, vol ) )
    return photons

def photon_gun( location, dir, vol ):
    photons = []
    ray = go.ray( location, dir )
    photons.append( ph.photon( ray, vol ) )
    return photons
