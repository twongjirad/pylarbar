# photonweights_gpu: module that calculates probability photon escapes
#      bar at the detector end. uses routines to be run on a GPU.

# This file is part of pylarbar
# Copyright 2015 by Taritree Wongjirad (taritree@mit.edu)

# pylarbar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# pylarbar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pylarbar.  If not, see <http://www.gnu.org/licenses/>.

import os
#os.environ["PYOPENCL_CTX"]='1'
import numpy as np
#import pylarbar.generate as gen
import fresnel as fr
import pyopencl as cl
import time as ti

__numerical_gpu_func_verbose__ = 0

def get_photonweights_at_source_gpu( ctx, queue, 
                                     nphotons, source_pos, det_offset, 
                                     dir_np, dir_buf, 
                                     barlength, barwidth, barheight, 
                                     n_in, n_out, surface_loss, coating_diff, bulk_att_length ):

    # Get GPU Info
    dev = ctx.get_info( cl.context_info.DEVICES )
    max_dims = dev[0].get_info( cl.device_info.MAX_WORK_ITEM_DIMENSIONS )
    max_work_items_per_dim = dev[0].get_info( cl.device_info.MAX_WORK_ITEM_SIZES )
    max_work_group_size = dev[0].get_info( cl.device_info.MAX_WORK_GROUP_SIZE )
    max_compute_units = dev[0].get_info( cl.device_info.MAX_COMPUTE_UNITS )
    max_mem_alloc_size = dev[0].get_info( cl.device_info.MAX_MEM_ALLOC_SIZE )

    floatsneeded = nphotons*(3+3+1) + 3 # 3:dir, 3:pos, 1:weight, 3:bardims
    bytesneeded = floatsneeded*32.0
    if __numerical_gpu_func_verbose__:
        print "Number of bytes needed: ",bytesneeded/1.0e9

    pos_np = np.array( source_pos.tolist(), dtype=np.float32 )
    weights_np = np.zeros( nphotons, dtype=np.float32 )
    
    bar_np = np.array( [ barlength, barwidth, barheight ], dtype=np.float32 )
    
    # future: use clarray functions
    mf = cl.mem_flags
    pos_g = cl.Buffer( ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=pos_np )
    bar_g = cl.Buffer( ctx, mf.READ_ONLY | mf.COPY_HOST_PTR, hostbuf=bar_np )
    weights_g = cl.Buffer( ctx, mf.WRITE_ONLY, weights_np.nbytes )
    
    # build kernel
    kernelfilepath = os.path.dirname(__file__)+"/../cl/weightcalc.cl"
    kernelfile = open(kernelfilepath,'r')
    prg = cl.Program(ctx, kernelfile.read() ).build()

    # run kernel
    workdims = (nphotons,1,1)
    prg.weightcalc( queue, workdims, None,
                    np.float32(surface_loss), np.float32(coating_diff), np.float32(bulk_att_length),
                    np.float32(n_in), np.float32(n_out), np.float32(det_offset),
                    dir_buf, pos_g, bar_g,
                    weights_g )
    cl.enqueue_copy( queue, weights_np, weights_g )
    #print weights_np
    totweight = np.sum( weights_np )
    return totweight
