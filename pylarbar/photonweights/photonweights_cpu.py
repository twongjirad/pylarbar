# photonweights_cpu: module that calculates probability photon escapes
#      bar at the detector end. uses routines to be run on the CPU.

# This file is part of pylarbar
# Copyright 2015 by Taritree Wongjirad (taritree@mit.edu)

# pylarbar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# pylarbar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pylarbar.  If not, see <http://www.gnu.org/licenses/>.

import os,sys
import numpy as np
#import pylarbar.generate as gen
import fresnel as fr
import pyopencl as cl
import time as ti

__numerical_cpu_func_verbose__ = 0 # for debug

# =======================================================
# CPU IMPLEMENTATION

def get_nbounces( photon, barlength, barwidth, barthick, sourcepos, detend ):
    # assumes bar lies on x-axis
    # counts number of bounces photon makes before hiting detector at -x end

    w = [ barlength, barwidth, barthick] # dimensions
    n = [ np.array( [1.0, 0.0, 0.0] ), np.array( [0.0, 1.0, 0.0] ), np.array( [0.0, 0.0, 1.0] ) ] # norms

    v = photon.current_ray.dir
    o = photon.current_ray.start

    bounces = []
    angles = []

    wrongend = False
    for i in xrange(0,3): # loop over dimensions
        # get incidence angles
        costh = np.dot( v, n[i] )
        if costh<0:
            costh = np.dot( v, -1*n[i] )
        theta_i = np.arccos( costh )
        angles.append( theta_i )

        # get number of bounces
        zdist_per_step = np.tan(theta_i)*w[i]
        if v[0]*detend>0:
            zdist_total = np.fabs(detend-sourcepos[0])
        else:
            zdist_total = 2*barlength - np.fabs(detend-sourcepos[0])
            wrongend = True
        if i!=0:
            # y,z
            bounces.append( zdist_total/zdist_per_step )
        else:
            # x
            if wrongend:
                bounces.append(1)
            else:
                bounces.append(0)
    return bounces, angles, wrongend

def calc_dist_traveled( photon, barlength, barwidth, barthick, sourcepos, detend ):
    # do the dumbest thing, essentially ray trace...
    w = [ barlength, barwidth, barthick]

def get_photonweight( photon, barlength, barwidth, barthick, n_in, n_out, source_pos, detend, coefficient_eff_loss, pct_coating_end_diff, bulk_att_length ):
    # calculates weight of photon traveling in bar and finally exiting out of -x end of bar (where detector lies)
    __verbose__ = __numerical_cpu_func_verbose__
    # get bounces in each dimensions, other information
    bounces,incidence_angles, wrongend = get_nbounces( photon, barlength, barwidth, barthick, source_pos, detend )
    totalbounces = sum(bounces)

    if __verbose__:
        print "bounces: ",bounces
        print np.array(incidence_angles)*(180/np.pi)

    # contribution from fresnel partial reflection
    Ry,Ty = fr.calc_fresnel( n_in, n_out, np.cos( incidence_angles[1] ) )
    logweighty = int(bounces[1])*np.log(Ry)
    if __verbose__:
        print "Ry: ",Ry," from angle ",incidence_angles[1]*180/np.pi
    
    Rz,Tz = fr.calc_fresnel( n_in, n_out, np.cos( incidence_angles[2] ) )
    logweightz = int(bounces[2])*np.log(Rz)
    if __verbose__:
        print "Rz: ",Rz," from angle ",incidence_angles[2]*180/np.pi
    
    logweightx = 0.0
    Rx = 1.0
    if wrongend:
        # need reflection from end of bar
        Rx,Tx = fr.calc_fresnel( n_in, n_out, np.cos( incidence_angles[0] ) )
        logweightx = np.log(Rx)
        totalbounces += 1

    # contribution to weight from surface loss
    logweightloss = int(totalbounces)*np.log( 1-coefficient_eff_loss )
    if __verbose__:
        print "log weights: ",logweighty,logweightz,logweightloss

    # weight from coating difference
    if pct_coating_end_diff>0:
        coat_pct_diff = pct_coating_end_diff*(source_pos[0] + 0.5*barlength)/barlength
    else:
        coat_pct_diff = pct_coating_end_diff*(0.5*barlength-source_pos[0])/barlength
    logweight_coat = np.log(1 + coat_pct_diff)

    # weight from bulk loss
    path_per_x = np.fabs(1.0/photon.current_ray.dir[0])
    if photon.current_ray.dir[0]>0:
        xtotal = barlength + (0.5*barlength-source_pos[0])
    else:
        xtotal = 0.5*barlength+source_pos[0]
    logweightbulk = -xtotal*path_per_x/bulk_att_length

    # contribution from transmission
    Rx, Tx = fr.calc_fresnel( n_in, n_out, np.cos( incidence_angles[0] ) )

    # total weight
    weighttotal = np.exp(logweighty+logweightz+logweightx+logweightloss+logweight_coat+logweightbulk)
    weighttotal *= Tx

    if __verbose__:
        print "total weight: ",weighttotal
    return weighttotal

def get_photonweights_cpu( photons, barlength, barwidth, barthick, n_in, n_out, source_pos, detend, surface_loss, pct_coating_end_diff, bulk_att_length ):
    photonsobserved = 0.
    for photon in photons:
        weight = get_photonweight( photon, barlength, barwidth, barthick, n_in, n_out, source_pos, detend, surface_loss, pct_coating_end_diff, bulk_att_length )
        #print weight
        photonsobserved += weight    
    return photonsobserved

    
