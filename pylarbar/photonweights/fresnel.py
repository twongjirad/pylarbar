# fresnel: module that contains functions for calculating partal reflection 
#          probabilities using the Fresnel equations

# This file is part of pylarbar
# Copyright 2015 by Taritree Wongjirad (taritree@mit.edu)

# pylarbar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# pylarbar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pylarbar.  If not, see <http://www.gnu.org/licenses/>.

import os,sys
import numpy as np

def calc_fresnel( n_in, n_out, cos_i ):
    #print "fresnel: ",n_in, n_out, cos_i

    sin_i = np.sqrt(1-cos_i*cos_i) # incidence/reflectance
    sin_t = n_in*sin_i/n_out

    if sin_t>1.0:
        R = 1.0
        T = 0.0
    else:
        cos_t = np.sqrt(1-sin_t*sin_t)

        Rs = (n_in*cos_i - n_out*cos_t)/(n_in*cos_i + n_out*cos_t)
        #th1 = np.arcsin( sin_i )
        #th2 = np.arcsin( sin_t )
        #if th1+th2>0:
        #    Rs = np.sin( th1-th2 )/np.sin(th1+th2)
        #else:
        #    Rs = 0.0
        Rs *= Rs

        Rp = (n_in*cos_t - n_out*cos_i)/(n_in*cos_t + n_out*cos_i)
        #if th1+th2>0:
        #    Rp = np.tan( th1-th2 )/np.tan( th1+th2 )
        #else:
        #    Rp = 0.0
        Rp *= Rp

        #print th1,th2,Rs,Rp

        R = (Rs+Rp)/2
        T = 1-R


    return R,T

if __name__ == "__main__":
    import ROOT as rt

    n_air = 1.0
    n_argon = 1.23
    n_acrylic = 1.49

    f = rt.TFile("out_fresnel.root", "RECREATE")
    
    hairR = rt.TH1D( "hairR","", 200, 0, 90.0 )
    hairT = rt.TH1D( "hairT","", 200, 0, 90.0 )
    hlarR = rt.TH1D( "hlarR","", 200, 0, 90.0 )
    hlarT = rt.TH1D( "hlarT","", 200, 0, 90.0 )
    for x in xrange(1,hairR.GetNbinsX()+1):
        theta = (90.0)/float(hairR.GetNbinsX())*(x-1)*np.pi/180.0
        costh = np.cos(theta)
        Rair,Tair = calc_fresnel( n_acrylic, n_air, costh )
        Rlar,Tlar = calc_fresnel( n_acrylic, n_argon, costh )
        hairR.SetBinContent( x, Rair )
        hlarR.SetBinContent( x, Rlar )
        hairT.SetBinContent( x, Tair )
        hlarT.SetBinContent( x, Tlar )
        
    c = rt.TCanvas("c","c",800,400)
    hairR.Draw()
    hairT.SetLineStyle(2)
    hairT.Draw("same")
    
    hlarR.SetLineColor(rt.kRed)
    hlarT.SetLineColor(rt.kRed)
    hlarR.Draw("same")
    hlarT.SetLineStyle(2)
    hlarT.Draw("same")

    print "critical angles: "
    print "  air: ",np.arcsin( n_air/n_acrylic )*180.0/np.pi," (",np.arcsin( n_air/n_acrylic ),"radians)"
    print "  LAr: ",np.arcsin( n_argon/n_acrylic )*180/np.pi," (",np.arcsin( n_argon/n_acrylic ),"radians)"
    f.Write()
    raw_input()
