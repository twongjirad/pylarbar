//-*- mode: c-*-
/*
# genphotons.cl: kernel functions to generate photon directions
#    using GPU.

# This file is part of pylarbar
# Copyright 2015 by Taritree Wongjirad (taritree@mit.edu)

# pylarbar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# pylarbar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pylarbar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Random123/threefry.h"

__kernel void gen_photon_dir( uint baseoffset, __global float* dir )
{
   uint pid = get_global_id(0); 

   // random goes here
   threefry4x32_key_t k = {{pid, 0xdecafbad, 0xfacebead, 0x12345678}};
   threefry4x32_ctr_t c = {{baseoffset, 0xf00dcafe, 0xdeadbeef, 0xbeeff00d}};
   union {
      threefry4x32_ctr_t c;
      int4 i;
    } u;

   u.c = threefry4x32(c, k);

   uint rand1 = u.i.x;
   uint rand2 = u.i.y;

   float frand1 = float(rand1)/float(4294967295);
   float frand2 = float(rand2)/float(4294967295);
   
   float phi = 2*3.14159*frand1;
   float costh = frand2 - 1.0;
   float sinth = sqrt( 1 - costh*costh );

   float photon_dir[3];
   float photon_pos[3];

   photon_dir[0] = cos(phi)*sinth;
   photon_dir[1] = sin(phi)*sinth;
   photon_dir[2] = costh;

   for (int v=0; v<3; v++) {
     dir[ 3*pid + v ] = photon_dir[v];
   }
}

