//-*- mode: c-*-
/*
# weightcalc.cl: kernel function to calculate photon weights

# This file is part of pylarbar
# Copyright 2015 by Taritree Wongjirad (taritree@mit.edu)

# pylarbar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# pylarbar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pylarbar.  If not, see <http://www.gnu.org/licenses/>.
*/

float heaviside( float x ); // heaviside function, used to unroll logic
float fresnel_weights( float s_i, float n_i, float n_o ); // calculate partial reflection prob. using fresnel equations

// Extension of model described in 
// BJP. Jones, JINST 8 (2013) C10015 (http://arxiv.org/abs/1307.6906)
// Written for
// Moss et al., Improved TPB-coated Light Guides for Liquid Argon TPC Light Detection Systems, http://arxiv.org/abs/1410.6256

__kernel void weightcalc( const float eff_surface_loss,
                          const float eff_coating_diff,
                          const float bulk_att_length,
                          const float n_in,
                          const float n_out,
                          const float det_offset,
                          __global const float* photon_dir,
                          __global const float* photon_pos,
                          __global const float* bardims,
                          __global float* weight )
 {
  // memory tally (12/1/2014
  // ints: 3
  // floats: 44
  // est. total: 1.5 kbytes + inline functions
  int i,v;
  int pid = get_global_id(0);
  float totweight = 0.0;
  float R, T, c_th, c_thi, s_th, detpos, stepsize, nearbounces, farbounces, angle;
  float dist_to_near, dist_to_far;
  float weight_fres, weight_surface, weight_coating, weight_trans, weight_bulk;
  float coating_factor_forward, coating_factor_backward;
  float path_per_x;
  float dir[3];
  float pos[3];
  float norms[3][3] = { {1.0f, 0.0f, 0.0f},
                        {0.0f, 1.0f, 0.0f},
                        {0.0f, 0.0f, 1.0f} };
  float nbounces[3];
  float angles[3];
  float freR[3];
  for ( v=0; v<3; v++ ) {
   dir[v] = photon_dir[ 3*pid + v ];
   pos[v] = photon_pos[ v ];
  }

  // note we calculate log(w) below
  // function returns w

  // calculate incidence angles for x, y, z facing planes
  for (i=0; i<3; i++) {
    c_th = 0.;
    c_thi = 0.;
    for (v=0; v<3; v++) {
      c_th += norms[i][v]*dir[v];
      c_thi += -norms[i][v]*dir[v];
    }
    c_th = heaviside( c_th )*c_th + heaviside( c_thi )*c_thi; // comment out to compare to BJPJ model
    angles[i] = acos( c_th );
  }

  // number of bounces
  detpos = -0.5f*bardims[0]+det_offset;
  for (i=1; i<3; i++) {
    angle = angles[i];
    stepsize = fabs(tan(angle)*bardims[i]);
    nearbounces = fabs(pos[0] - detpos )/stepsize;
    farbounces  = (2*bardims[0] - fabs(pos[0] - detpos))/stepsize;
    nbounces[i] = heaviside( dir[0] )*farbounces + heaviside( -dir[0] )*nearbounces;
  }
  nbounces[0] = heaviside( dir[0] );

  // calculate weight from partial reflection
  totweight = 0.0;
  for (i=0; i<3; i++) { // i=1: incorrect, but change for BJPJ Model comparison
    // apply fresnel weights
    angle = angles[i];
    s_th = sin( angle );
    R = fresnel_weights( s_th, n_in, n_out );
    freR[i] = R;
    weight_fres = trunc( nbounces[i] )*log(R);
    totweight += weight_fres;

    // apply surface losses per bounce
    weight_surface = trunc( nbounces[i] )*log(1.0-eff_surface_loss);
    totweight += weight_surface;
  }


  // apply linear coating gradient effect.
  // simple reweighting based on position
  dist_to_near = (pos[0]+0.5*bardims[0])/bardims[0];
  dist_to_far  = (0.5*bardims[0]-pos[0])/bardims[0];
  // linear
  coating_factor_forward = heaviside(eff_coating_diff)*(eff_coating_diff)*dist_to_near;
  coating_factor_backward = heaviside(-eff_coating_diff)*(-eff_coating_diff)*dist_to_far;
  // quad
  //coating_factor_forward = heaviside(eff_coating_diff)*(eff_coating_diff)*dist_to_near*dist_to_near;
  //coating_factor_backward = heaviside(-eff_coating_diff)*(-eff_coating_diff)*dist_to_far*dist_to_far;
  weight_coating = log(1.0+coating_factor_forward+coating_factor_backward);
  totweight += weight_coating;

  // weight due to bulk loss
  path_per_x = fabs(1.0/dir[0]);
  weight_bulk = (heaviside(dir[0])*(dist_to_far+1.0)+heaviside(-dir[0])*dist_to_near)*path_per_x*bardims[0];
  weight_bulk /= bulk_att_length;
  totweight -= weight_bulk;

  totweight = exp(totweight);

  // transmission out of bar
  angle = angles[0];
  s_th = sin(angle);
  R = fresnel_weights( s_th, n_in, 1.46 ); // acrylic/glass transition
  T = 1.0-freR[0];
  R = freR[0];
  weight_trans = T/(1.0-R*R);
  weight_trans = fmax( weight_trans, 0.0f );
  totweight *= weight_trans;

  // final weight
  weight[pid] = totweight;

};

float heaviside( float x ) {
   float out = atan(x)+1.0;
   out = trunc( out );
   return out;
};

float fresnel_weights( float s_i, float n_i, float n_o ) {

   float s_tt = n_i*s_i/n_o;
   float s_t = fmin( s_tt, 1.0f );
   float c_t = sqrt( 1-s_t*s_t );
   float c_i = sqrt( 1-s_i*s_i );
   float Rs = ( n_i*c_i - n_o*c_t)/(n_i*c_i + n_o*c_t );
   float Rp = ( n_i*c_t - n_o*c_i)/(n_i*c_t + n_o*c_i );

   float Rf = 0.5*( Rs*Rs + Rp*Rp );
   Rf = fmin( Rf, 1.0f );
   return Rf;
};


