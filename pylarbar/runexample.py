# runexample: function that shows how to config and run the package

# This file is part of pylarbar
# Copyright 2015 by Taritree Wongjirad (taritree@mit.edu)

# pylarbar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# pylarbar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pylarbar.  If not, see <http://www.gnu.org/licenses/>.

from pylarbar.barmodel import barmodel

def run_example( wrootplot=False, use_gpu = True, use_cpu = False, points=None, use_sparse = False ):
    # Example
    #use_gpu = True     # Use OpenCl
    #use_cpu = False    # Use CPU
    #use_sparse = False # True: calculate bar response with sparce number of points

    mymodel = barmodel( use_gpu, use_cpu )

    # specify bar parameters
    mymodel.barthick = 0.25*2.54
    mymodel.barwidth = 1.0*2.54
    mymodel.barlength = 50.0*2.54
    mymodel.det_offset = 0.0*2.54

    n_air = 1.00  # air
    n_argon = 1.23   # argon
    n_acrylic = 1.49 # acrylic

    mymodel.n_bar = n_acrylic
    mymodel.n_env = n_argon
    #mymodel.n_env = n_air
    mymodel.det_offset = 0.0

    mymodel.surface_loss = 0.00 # loss per bounce
    mymodel.coating_diff = 00.00 # ten percent difference between near and far ends, with near end thinner
    mymodel.bulk_att_length = 1.0e6 # 10 meters

    # number of photons to simulate
    mymodel.nphotons = 100000

    # points to caclulate on
    if points!=None:
        source_dists = points
    else:
        if not use_sparse:
            source_dists = []
            for x in xrange(1,51):
                source_dists.append( float(x) )
        else:
            source_dists = [ 0.5, 1.0, 2.0, 3.0, 4.0, 5.0 ]
            for x in xrange(10,51,5):
                source_dists.append( float(x) )
        
    eff_curve = mymodel.calc_light_yield_curve( source_dists )
    print "light yield curve: ",eff_curve

    # Bad
    if wrootplot:
        import ROOT as rt
        f = rt.TFile("output_runexample.root","recreate")
        g = rt.TGraph( len(source_dists) )
        for n,source_pos_fromend in enumerate(source_dists):
            g.SetPoint( n, source_pos_fromend, eff_curve[n] )
        c = rt.TCanvas("c","c",800,600)
        g.Write("loss_curve")
        g.Draw("AL")
        g.GetYaxis().SetRangeUser(0,0.5)
        c.Update()
        raw_input()
    
if __name__=="__main__":
    run_example(  wrootplot=True, use_gpu = True, use_cpu = False, points=None, use_sparse = False )
