# geomobjs: classes that define geometrical entities

# This file is part of pylarbar
# Copyright 2015 by Taritree Wongjirad (taritree@mit.edu)

# pylarbar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# pylarbar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pylarbar.  If not, see <http://www.gnu.org/licenses/>.

import os,sys
import numpy as np

class ray:
    def __init__( self, start, dir ):
        self.start = start
        self.dir = dir/np.linalg.norm(dir)

class rectangle:
    def __init__( self, name, center, halfwidth, halfheight, norm, wdir ):
        self.name = name
        self.center = center
        self.halfheight = halfheight
        self.halfwidth  = halfwidth
        self.norm = norm
        self.wdir = wdir
        self.hdir = np.cross(norm,wdir)
        self.planes = [self]
        
class box:
    def __init__( self, name, center, halfheight, halfwidthx, halfwidthy, rot=np.eye(3) ):
        self.name = name
        self.center = center
        self.ez = rot.dot( np.array( [0,0,1] ) )
        self.ex = rot.dot( np.array([1,0,0]) )
        self.ey = rot.dot( np.array([0,1,0]) )
        self.halfheight = halfheight
        self.halfwidthx = halfwidthx
        self.halfwidthy = halfwidthy
        self.bottom = rectangle( name+".bottom",
                                 np.array( [0,0,-halfheight]+center ),
                                 halfwidthx,
                                 halfwidthy,
                                 rot.dot( np.array([0,0,-1.0]) ),
                                 rot.dot( np.array([1.0,0,0]) ) )
        self.top    = rectangle( name+".top",
                                 np.array( [0,0,halfheight]+self.center ),
                                 halfwidthx,
                                 halfwidthy,
                                 rot.dot( np.array([0,0,1.0]) ),
                                 rot.dot( np.array([1.0,0,0]) ) )

        # short side
        self.sidey1  = rectangle( name+".y-",
                                  np.array( [0.0,-halfwidthy,0.0] ),
                                  halfwidthx,
                                  halfheight,
                                  rot.dot( np.array([0.0, -1.0, 0.0]) ),
                                  rot.dot( np.array([1.0, 0.0, 0.0]) ) )
        self.sidey2  = rectangle( name+".y+",
                                  np.array( [0.0,halfwidthy,0.0] ),
                                  halfwidthx,
                                  halfheight,
                                  rot.dot( np.array([0.0, 1.0, 0.0]) ),
                                  rot.dot( np.array( [-1.0, 0.0, 0.0] ) ) )
    
        # long sides
        self.sidex1  = rectangle( name+".x-",
                                  np.array( [-halfwidthx,0.0,0.0] ),
                                  halfwidthy,
                                  halfheight,
                                  rot.dot( np.array([-1.0, 0.0, 0.0]) ),
                                  rot.dot( np.array([0.0, -1.0, 0.0]) ) )
        self.sidex2  = rectangle( name+".x+",
                                  np.array( [halfwidthx,0.0,0.0] ),
                                  halfwidthy,
                                  halfheight,
                                  rot.dot( np.array([1.0, 0.0, 0.0]) ),
                                  rot.dot( np.array([0.0, 1.0, 0.0]) ) )
        self.planes = [ self.top, self.bottom, self.sidex1, self.sidex2, self.sidey1, self.sidey2 ]

class volume:
    def __init__( self, name, parent, index_refraction, center, halfheight, halfwidthx, halfwidthy, rot=np.eye(3) ):
        self.name = name
        self.n = index_refraction
        self.box = box( name+"_box", center, halfheight, halfwidthx, halfwidthy, rot )
        self.parent = parent
        if parent != None:
            parent.adoptme( self )
        self.children = []
    def adoptme( self, child ):
        if child not in self.children:
            self.children.append( child )
    def numchildren(self):
        return len(self.children)
    def ischild( self, child ):
        if child in self.children:
            return True
        else:
            return False
    def getchildren(self):
        return list(self.children)
    def getparentvolumes(self):
        if self.parent != None:
            vols = self.parent.getchildren()
            vols.append(self.parent)
            return vols
        else:
            return list(self)
