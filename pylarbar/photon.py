import os,sys
import numpy as np

class photon():
    def __init__(self, init_ray, vol):
        self.origin = np.copy( init_ray.start )
        self.current_ray = init_ray
        self.path = [ init_ray.start ]
        self.weights = [1.0]
        self.incidenceangles = []
        self.current_volume = vol
        self.escaped = False
        self.lost = False
        self.detected = False
    def printinfo(self):
        print "photon ray: pos=",self.current_ray.start," dir=",self.current_ray.dir,
        print " weight=",self.getweight()
        print " is tracking=",self.istracked()
    def getweight(self):
        return reduce(lambda x, y: x*y, self.weights, 1.0)
    def istracked(self):
        if self.escaped or self.lost or self.detected:
            return False
        else:
            return True
