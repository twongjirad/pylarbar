# barmodel: class to hold model parameters and interface to calculation 
# of fraction photon yield per source position

# This file is part of pylarbar
# Copyright 2015 by Taritree Wongjirad (taritree@mit.edu)

# pylarbar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# pylarbar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pylarbar.  If not, see <http://www.gnu.org/licenses/>.

import calcbaryield as barf

class barmodel:
    def __init__(self, use_gpu=True, use_cpu=False):
        if use_gpu:
            try:
                import pyopencl as cl
            except:
                raise ImportError('Model asked to use OpenCL, but could not import pyopencl')

        self.barlength = None       # dimension along x axis
        self.barwidth = None        # dimension along y axis
        self.barthick = None        # dimension along z axis
        self.det_offset = None      # detector offset: when using PMT holder, amount of bar inside same volume as PMT
        self.n_bar = None           # index of refraction inside bar
        self.n_env = None           # index of refraction outside bar
        self.surface_loss = None    # fraction of photons lost per bounce
        self.coating_diff = None    # fraction change in coating eff./thickness/whatever from far to near end
        self.bulk_att_length = None # bulk attenuation length
        self.nphotons = None        # number of photons to numerical calculate light yeild
        self.use_gpu = use_gpu      # switches to use or not use gpu
        self.use_cpu = use_cpu      # switches to use or not use cpu. if both gpu and cpu true, then will output some comparisons between calculations for debugging/profiling
    def isready(self):
        # using __dict__ is probably a no-no
        for var in self.__dict__:
            if self.__dict__[var]==None:
                print "variable '%s' not set"%(var)
                return False
        return True
    def calc_light_yield_curve(self, source_positions):
        if not self.isready():
            print "a parameter in the model has not been set. return None."
            return None
        return barf.fractional_yield_curve( self, source_positions, self.use_gpu, self.use_cpu )

