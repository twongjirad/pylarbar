# calcbaryield: module with functions that return fraction photon yield per source position

# This file is part of pylarbar
# Copyright 2015 by Taritree Wongjirad (taritree@mit.edu)

# pylarbar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.

# pylarbar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pylarbar.  If not, see <http://www.gnu.org/licenses/>.

import os,sys
import numpy as np
import pylarbar.genphotons as gen
import time as ti

__numerical_func_verbose__ = 0

# =======================================================
# COMMON IMPLEMENTATION

def calc_yield_curve( barlength, barwidth, barthick, 
                      n_bar, n_env, det_offset, 
                      surface_loss, coating_diff, bulk_att_length, 
                      source_dists, nphotons, use_gpu=True, use_cpu=False, compare_gpu_cpu=True ):
    
    # this strongly assumes a coordinate system.
    # bar's length runs along the x-axis.
    # the short width is along z
    # the center of the bar is at (0,0,0)

    if use_gpu:
        import pyopencl as cl
        import pylarbar as plb
        import pylarbar.photonweights.photonweights_gpu as gpuweights
        ctx = cl.create_some_context()
        queue = cl.CommandQueue(ctx)
    if use_cpu:
        import pylarbar.photonweights.photonweights_cpu as cpuweights

    detend = -0.5*barlength + det_offset
    
    eff_out = []

    total_cpu_time = 0.0
    total_gpu_time = 0.0

    for n,source_pos_fromend in enumerate(source_dists):
        source_pos = np.array( [ -0.5*barlength + source_pos_fromend, 0.0, 0.5*barthick ] )
        photonsobserved = 0.
        copy_gpu_dir = True

        if use_gpu:
            gpu_start = ti.time()
            dir_buf, dir_np = gen.photongen_at_source_gpu( ctx, queue, nphotons, source_pos, 0, copy_gpu_dir )
            photonsobserved_gpu = gpuweights.get_photonweights_at_source_gpu( ctx, queue, nphotons, source_pos, det_offset, 
                                                                          dir_np, dir_buf, barlength, barwidth, barthick, n_bar, n_env, 
                                                                          surface_loss, coating_diff, bulk_att_length )
            gpu_end = ti.time()
            if __numerical_func_verbose__:
                print "GPU run time: ",gpu_end-gpu_start
                total_gpu_time += gpu_end-gpu_start
            if not use_cpu:
                photonsobserved = photonsobserved_gpu
        if use_cpu:
            cpu_start = ti.time()
            if use_gpu and copy_gpu_dir:
                photons = gen.generate_photons_on_bar( source_pos, nphotons, None, dir_np )        
            else:
                photons = gen.generate_photons_on_bar( source_pos, nphotons, None )        
                photonsobserved = cpuweights.get_photonweights_cpu( photons, barlength, barwidth, barthick, n_bar, n_env, 
                                                                    source_pos, detend, surface_loss, coating_diff, bulk_att_length )
            cpu_end = ti.time()
            if __numerical_func_verbose__:
                print "CPU run time: ",cpu_end-cpu_start
                total_cpu_time += cpu_end-cpu_start

        if __numerical_func_verbose__:
            print "distance from end: ",source_pos_fromend
        if use_gpu and use_cpu and compare_gpu_cpu:
            print "photons observed (cpu): %.5f"%(photonsobserved)
            print "photons observed (GPU): %.5f"%(photonsobserved_gpu)
            print "efficiency: ",photonsobserved/nphotons
        eff_out.append( photonsobserved/nphotons )
    if use_gpu and use_cpu and compare_gpu_cpu:
        print "gpu run time: ",total_gpu_time
        print "cpu run time: ",total_cpu_time
    return eff_out

def fractional_yield_curve( model, source_dists, use_gpu=True, use_cpu=False, compare_gpu_cpu=True ):
    return calc_yield_curve( model.barlength, model.barwidth, model.barthick,
                             model.n_bar, model.n_env, model.det_offset,
                             model.surface_loss, model.coating_diff, model.bulk_att_length,
                             source_dists, model.nphotons, use_gpu=use_gpu, use_cpu=use_cpu, compare_gpu_cpu=compare_gpu_cpu )
    
    
