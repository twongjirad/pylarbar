### pylarbar

Numerical model that calculates the amount of light output at the end of 
an acrylic light guide in response to an UV source at some position along the bar.
For a description of the model, see BJP Jones, JINST C10015 (2013).  
Calculation implemented for use on both CPU and GPU.

## Example Usage

See pylarbar/runexample.py

## Dependencies

* numpy: http://www.numpy.org
* pyopencl: http://documen.tician.de/pyopencl/. For GPU acceleration.
* Random123: http://www.deshawresearch.com/downloads/download_random123.cgi/. Random number generator. Included in repo.

## GPU-acceleration
Tested on Early 2011 15" MacBook Pro (2.3 GHz Intel Core i7, AMD Radeon HD 6750M). Running 50K toy photons.

* CPU: 71.997 sec
* GPU:  0.164 sec


